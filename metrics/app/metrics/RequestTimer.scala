package metrics

import java.util.concurrent.TimeUnit
import javax.inject.Inject

import akka.stream.Materializer
import nl.grons.metrics.scala.MetricName
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.mvc._

import scala.concurrent.Future

/**
  * Created by ellie on 9/15/16.
  */
class RequestTimer @Inject() (implicit val mat: Materializer) extends Filter with Instrumented {

  /**
    * Timer measures the time between request sent and request returned
    */
  val applicationName ="pbp-api"
  private[this] val requestTimer = metrics.timer("all")
  private[this] val requestTimer200 = metrics.timer("status.200")
  private[this] val requestTimer400 = metrics.timer("status.400")
  private[this] val requestTimer404 = metrics.timer("status.404")
  private[this] val requestTimer500 = metrics.timer("status.500")
  override lazy val metricBaseName = MetricName(s"$applicationName.requests")


  def apply(nextFilter: RequestHeader => Future[Result])
           (requestHeader: RequestHeader): Future[Result] = requestTimer.timeFuture  {

    val startTime = System.currentTimeMillis

    nextFilter(requestHeader).map { result =>

      val endTime = System.currentTimeMillis
      val requestTime = endTime - startTime

      result.header.status match {
        case 200 => requestTimer200.update(requestTime, TimeUnit.MILLISECONDS)
        case 400 => requestTimer400.update(requestTime, TimeUnit.MILLISECONDS)
        case 404 => requestTimer404.update(requestTime, TimeUnit.MILLISECONDS)
        case 500 => requestTimer500.update(requestTime, TimeUnit.MILLISECONDS)
        case _ =>
      }

      result.withHeaders("Request-Time" -> requestTime.toString)
    }
  }
}
