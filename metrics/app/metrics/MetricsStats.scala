package metrics

import nl.grons.metrics.scala.MetricName
import play.api.mvc.{Action, ActionBuilder, Request, Result}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * Created by giannis on 18/1/2017.
  */
trait MetricsStats  extends Instrumented{

  val applicationName ="pbp-api"
  override lazy val metricBaseName = MetricName(s"$applicationName.requests.endpoint")


  case class MetricAction[A](name:String)(action: Action[A]) extends Action[A]  {
    val timer = metrics.timer(name)
    def apply(request: Request[A]): Future[Result] = {
      timer.timeFuture(action(request))
    }
    lazy val parser = action.parser
  }



}
