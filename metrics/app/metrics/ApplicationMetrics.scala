package metrics

import java.io.File
import java.lang.management.ManagementFactory
import java.net.InetSocketAddress
import java.util.Locale
import java.util.concurrent.TimeUnit

import com.codahale.metrics._
import com.codahale.metrics.graphite.Graphite
import com.codahale.metrics.health.HealthCheckRegistry
import com.codahale.metrics.jvm._
import com.typesafe.config.ConfigFactory
import nl.grons.metrics.scala.{CheckedBuilder, InstrumentedBuilder, MetricName}
import org.scalatest.time.Minutes
import play.api.Configuration

object ZenoApiApplication  {
  val config = new Configuration(ConfigFactory.load())
  val csvEnabled = config.getBoolean("reporters.csv.enabled").getOrElse(true)
  val hostname = config.getString("graphite.hostname").getOrElse("")
  val port = config.getInt("graphite.port").getOrElse(9000)
  val prefix = config.getString("graphite.prefix").getOrElse("dev\n")
  /** The application wide metrics registry. */
  val metricRegistry = new MetricRegistry()
  val healthRegistry = new HealthCheckRegistry()

  val applicationName ="pbp-api"
  /** JVM metrics*/
  val memoryUsageGaugeSet: MemoryUsageGaugeSet = new MemoryUsageGaugeSet()
  val garbageCollectorMetricSet : GarbageCollectorMetricSet = new GarbageCollectorMetricSet()
  val fileDescriptorRatioGauge : FileDescriptorRatioGauge = new FileDescriptorRatioGauge()
  val bufferPoolMetricSet : BufferPoolMetricSet = new BufferPoolMetricSet(ManagementFactory.getPlatformMBeanServer)
  val threadStatesGaugeSet : ThreadStatesGaugeSet = new ThreadStatesGaugeSet()
  val classLoadingGaugeSet : ClassLoadingGaugeSet = new ClassLoadingGaugeSet()

  metricRegistry.register(s"$applicationName.jvm.memoryUsage",memoryUsageGaugeSet)
  metricRegistry.register(s"$applicationName.jvm.garbageCollector",garbageCollectorMetricSet)
  metricRegistry.register(s"$applicationName.jvm.fileDescriptor", fileDescriptorRatioGauge)
  metricRegistry.register(s"$applicationName.jvm.bufferPool",bufferPoolMetricSet)
  metricRegistry.register(s"$applicationName.jvm.threadState",threadStatesGaugeSet)
  metricRegistry.register(s"$applicationName.jvm.classLoading",classLoadingGaugeSet)



  final val graphite = new Graphite(new InetSocketAddress(hostname, port))
  final val  reporter: CustomGraphiteReporter = CustomGraphiteReporter.forRegistry(metricRegistry)
    .prefixedWith(prefix)
    .convertRatesTo(TimeUnit.SECONDS)
    .convertDurationsTo(TimeUnit.MILLISECONDS)
    .filter(MetricFilter.ALL)
    .build(graphite);
  reporter.start(1, TimeUnit.MINUTES)
  
  //report to local csv
  if(csvEnabled){
    val customCsvReporter: CustomCsvReporter = CustomCsvReporter.forRegistry(metricRegistry)
      .formatFor(Locale.US)
      .convertRatesTo(TimeUnit.SECONDS)
      .convertDurationsTo(TimeUnit.MILLISECONDS)
      .build(new File("./metrics/data/"))
    customCsvReporter.start(5, TimeUnit.MINUTES)
  }


}



trait Instrumented extends InstrumentedBuilder with CheckedBuilder {
  val metricRegistry = ZenoApiApplication.metricRegistry
  val registry = ZenoApiApplication.healthRegistry
}





